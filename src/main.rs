use colored::*; // for terminal colors
use rand::Rng; // for random number generation
use std::cmp::Ordering;
use std::io; // for taking input // for taking input

fn main() {
    println!("Enter a number: "); // guessing game
    let secret_number = rand::thread_rng().gen_range(0, 10);
    //    println!("The secret number is , {}", secret_number);
    loop {
        let mut guess = String::new();
        io::stdin()
            .read_line(&mut guess)
            .expect("Failed to read line");
        println!("Number you have entered: {}", guess);

        let guess_int: u32 = match guess.trim().parse() {
            // no shadowing (guess_int, instead of guess)
            Ok(num) => num,
            Err(_) => {
                println!("PLEASE.....!! Enter a number ");
                continue;
            } //        expect("Expecting a number");
        };
        // io::stdin
        match guess_int.cmp(&secret_number) {
            Ordering::Equal => {
                println!("{}", "You won!! ".green());
                break;
            }
            Ordering::Greater => println!("{}", "Too large ".red()),
            Ordering::Less => println!("{}", "Too Small".yellow()),
        }
    }
}
